<!-- $theme: gaia -->
<!-- page_number: true -->

Kotlin 漫游
===

# ![](images/marp.png)


###### Created by Lingchao Xin ( [@douglarek](https://github.com/douglarek) )

---

# Kotlin 是什么

* 一门现代多平台 **应用型** **静态** 编程语言.

* Java 和 Android 平台 100% 互操作.

---

# 预备

![](https://d3nmt5vlzunoa1.cloudfront.net/wp-content/uploads/2014/02/logo_intellij_idea.jpg)

你所需要的唯一 IDE： [IntelliJ IDEA 2017.1.4](https://download.jetbrains.com/idea/ideaIC-2017.1.4.dmg)

---

# 起步

一个简单的 Hello World 程序：

```
public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello, World");
    }
}
```

```kotlin
fun main(args: Array<String>) {
   println("Hello World!")
}
```

---

## 基本语法

### 包

Kotlin 的包与 java 类似；不同点在于**包**和**目录**不需要对应；源码可以随意放置于文件系统的任何地方（虽然不提倡）：

```
package my.demo

import java.util.*

// ...
```

---

### 函数

Kotlin 中函数与 java 不同之处在于可以独立于 class 存在：

```
fun sum(a: Int, b: Int): Int {
    return a + b
}

fun main(args: Array<String>) {
    print("sum of 3 and 5 is ")
    println(sum(3, 5))
}
```

---

### 局部变量

不可变（只读），类似于 java `final`：

```
val a: Int = 1  // immediate assignment
val b = 2   // `Int` type is inferred
val c: Int  // Type required when no initializer is provided
c = 3       // deferred assignment
```

不同的地方是，kotlin 变量定义支持类型推断

可变：

```
var x = 5 // `Int` type is inferred
x += 1
```

---

### 注释

Kotlin 中注释与 java 类似，不同点在于支持块注释嵌套：

```kotlin
// This is an end-of-line comment

/* This is a block comment
   on multiple lines. */
```

---

### 字符串插值

与 java 字符串拼接相比，kotlin 插值的一个强大之处在于支持方法调用：

```kotlin
var a = 1
// simple name in template:
val s1 = "a is $a" 

a = 2
// arbitrary expression in template:
val s2 = "${s1.replace("is", "was")}, but now is $a"
```

---

### 条件表达式

kotlin 中 `if` 是表达式不是语句：

```
 val max = if (a > b) {
     return a
 } else {
     return b
}
```

所以 kotlin 虽然不支持三目运算符，可以用 `if` 实现：
```
if (a > b) a else b // java : a > b ? a : b
```

---

### 空值 `NULL`

如果一个引用可为空，那么必须显示的标记其类型为可空类型：

```
val a : String = null // compile error

val a : String? = null // ok
```

思考如下代码：

```kotlin
fun parseInt(str: String): Int? {
    // ...
}

parseInt(null) // ok or compile error ?
```

---

### 类型检查与自动转换

一旦使用 `is` 进行类型检查通过，对象的类型将不需要显示转化：

```
fun getStringLength(obj: Any): Int? {
    if (obj is String) {
        // 此处 obj 是 String 类型，所以可以直接 obj.length
        // 我们可以认为编译器帮我们进行类型转换
        return obj.length
    }

    // 但是此处 obj 类型依然是 Any，类型检查不会传递
    return null
}
```

---

### 循环

对应于 java 的 `for` 增强，kotlin `for` 更简洁：

```
val items = listOf("apple", "banana", "kiwi")
for (item in items) {
    println(item)
}
```

while 循环则与 java 无异：

```
val items = listOf("apple", "banana", "kiwi")
var index = 0
while (index < items.size) {
    println("item at $index is ${items[index]}")
    index++
}
```

---

### `when` 表达式

与 java `switch` 类似，kotlin `when` 提供了多分支条件的处理；

然而 `when` 可以支持更灵活的布尔条件：

```
fun describe(obj: Any): String =
when (obj) {
    1          -> "One"
    "Hello"    -> "Greeting"
    is Long    -> "Long"
    !is String -> "Not a string"
    else       -> "Unknown"
}

// 此处 `when` 是表达式（类型 String）
// 所以 describe 函数直接返回了 `when` 表达式
```

---

### 区间值 `Range`

区间迭代：

```
for (x in 1..5) {
    print(x)
}
```

区间步数迭代：

```kotlin
for (x in 1..10 step 2) {
    print(x)
}
for (x in 9 downTo 0 step 3) {
    print(x)
}
// 其实 `..` 看似语法糖，实际上和 `downTo` 一样是操作符方法（`Int.rangeTo`）
```

---

### 集合

迭代：

```
for (item in items) {
    println(item)
}
```

lambda：

```kotlin
fruits
.filter { it.startsWith("a") }
.sortedBy { it }
.map { it.toUpperCase() }
.forEach { println(it) }
```

---

### 基本类型

kotlin 中没有原始类型，一切皆对象

- 数字 `Double` `Float` `Long` `Int` `Short` `Byte`

- 字符 `Char` **非数字类型**

- 布尔 `Boolean`

- 数组 （Arrays） `val asc = Array(5, { i -> (i * i).toString() })`

- 字符串 （Strings）`val s = "Hello, world!\n"`

---

## 类与对象

### 类

` class` 定义类：

```kotlin
class Invoice {
}
// 如果 body 为空，大括号则可以省略
class Empty
```

---

#### 构造器

Koltin 类可以有一个主构造器和多个次构造器，主构造器是类声明头的一部分：
```kotlin
class Person constructor(firstName: String) {
}
// 这个简单的声明厉害了; 如果 constructor 关键字不需要可见性调整或注解，则可以省略
```
主构造器无法包含任何的代码，`init` 块可以执行初始化代码：

```kotlin
class Customer(name: String) {
    init {
        logger.info("Initialized with value ${name}")
    }
}
```

---

##### 次构造器
```kotlin
class Person(val name: String) {
    constructor(name: String, parent: Person) : this(name) {
        parent.children.add(this)
    }
}
```

如果既没有定义主构造器又没有次构造器，kotlin 会生成一个默认的无参主构造器；

如果不想要公开构造器：

```
class DontCreateMe private constructor () {
}
```

---

#### 创建实例

Kotlin 创建类的实例不需要 `new` 关键字：

```
val invoice = Invoice()

val customer = Customer("Joe Smith")
```

---

#### 类成员

- 构造器以及初始化块
- 方法（function）
- 属性（member）
- 嵌套类／内部类
- 伴生对象（Companion Object）// java 静态场所

---

### 继承

所有 Kotlin 类有一个公共的超类：`Any`

```
class Example // 隐式的继承自 Any

// Any 对应于 java.lang.Object, 但不等同
```

`Any` 不是 `java.lang.Object`，Any 只有三个方法：`equals`, `hashCode` 和 `toString`.

```
open class Base(p: Int)

class Derived(p: Int) : Base(p) // Base 右边的 p 是必须的
```

kotlin `class` 默认 `final`， 一个类可继承需要显示的使用 `open` 标注.

--- 

#### 方法覆写

一个可覆写的方法需显示的标记为 `open`:

```kotlin
open class Base {
    open fun v() {}
    fun nv() {}
}
class Derived() : Base() {
    override fun v() {} // override 是必须的区别于 java
}
```

一个标记为 `@override` 的方法可以在子类中被覆写，如果不想被子类覆写，需显示 `final` 标记：

```
open class AnotherDerived() : Base() {
    final override fun v() {}
}
```

---

#### 属性覆写

与方法覆写类似，不过属性可以覆写可变性：

```kotlin
interface Foo {
    val count: Int
}

// 可以覆写属性到子类主构造
class Bar1(override val count: Int) : Foo

class Bar2 : Foo {
    override var count: Int = 0 // count: val -> var
}
```

---

#### 覆写规则

如果 kotlin 类继承了多个超类的同一个成员，那么它必须覆写这个成员并提供自己的实现：

```
open class A {
    open fun f() { print("A") }
    fun a() { print("a") }
}

interface B {
    fun f() { print("B") } // 接口成员默认 open
    fun b() { print("b") }
}

class C() : A(), B {
    // f 必须覆写；因为编译器不知道需要调用哪个 f
    override fun f() {
        super<A>.f() // 调用 A.f()
        super<B>.f() // 调用 B.f()
    }
}
```

---

### 伴生对象
kotlin 没有静态方法，大多数情况下推荐使用包级别的函数替代。如果你确实想这么干，可以在类中伴随对象块实现：

```
companion object {
    fun sum(a: Int, b: Int): Int = a + b
}
  ```

---

## 属性与域

### 属性
```kotlin
class Address {
    var name: String = ...
    var street: String = ...
}

fun copyAddress(address: Address): Address {
    val result = Address()
    result.name = address.name
    result.street = address.street
    return result
}
```

---

### `Gettings` 和 `Setters`

公开属性 `val` 默认 `getters`，`var` 默认 `getters` 和 `setters`

自定义 `getters`：

```
val isEmpty get() = this.size == 0
```

自定义 `setters`：

```
var setterVisibility: String = "abc"
    private set // 禁用 setters
    
var setterWithAnnotation: Any? = null
    @Inject set // 通过 注入 进行 setters    
```

---

#### 备用域

kotlin 类不能有域

```
var counter = 0 // 初始化的值被写入了备用域
    set(value) {
        if (value >= 0) field = value
    }
```

#### 备用属性 

略

---

### 编译时常量

```
const val SUBSYSTEM_DEPRECATED: String = "This subsystem is deprecated"
```

### 延迟初始化属性

当 `var` 属性不在类实例化的时候赋值：

```
public class MyTest {
    lateinit var subject: TestSubject

    @SetUp fun setup() {
        subject = TestSubject()
    }

    @Test fun test() {
        subject.method()  // dereference directly
    }
}
```

---

## 接口

```
interface MyInterface { fun bar() }
```

### 实现接口

```
class Child : MyInterface {
    override fun bar() {
        // TODO
    }
}
```

### 接口属性

接口属性要么是抽象的要么是提供了默认访问实现，不可以有备用域.

---

### 解决覆写冲突 	(略过）

```
interface A {
    fun foo() { print("A") }
    fun bar()
}

interface B {
    fun foo() { print("B") }
    fun bar() { print("bar") }
}

class C : A {
    override fun bar() { print("bar") }
}

class D : A, B {
    override fun foo() {
        super<A>.foo()
        super<B>.foo()
    }

    override fun bar() {
        super<B>.bar()
    }
}
```

---

## 可见性修饰

`private`, `protected`, `internal(module)` 以及 `public`，默认 `public`.

```
class C private constructor(a: Int) { ... }
```

---

## 扩展

扩展 （extensions）是 Kotlin 最吸引人的特性之一，让扩展看起来好像是语言／库本身的一部分。

试想现在 `String` 现在想添加一个 `tail` 方法取尾字符，我们怎么做？

- 写个的自己的 `StringUtils` ? java 一般只能这样做

- Kotlin 的做法：

```
public fun String.tail(): Char {
  return this.reversed().first()
}

"12345".tail() // output: '5'
```

---

### [扩展是静态解析的（略过）](https://kotlinlang.org/docs/reference/extensions.html#extensions-are-resolved-statically)

---

### 可空 Receiver

```
fun Any?.toString(): String {
    if (this == null) return "null"
    // 智能转换
    return toString() // Any.toString
}

val a: String? = null

a.toString() // ok 
```

---

### 扩展属性

```kotlin
val <T> List<T>.lastIndex: Int
    get() = size - 1
    
val Foo.bar = 1 // error，因为扩展不会实际的插入备用域到 Foo
```

### 伴随对象扩展（略过）

### 扩展作用域（略过）

### 声明扩展作为成员

---
### 扩展的动机

在 java 中我们解决此类问题的思路是定义各种`*Utils`，甚至于 `java.util.Collections` 也是如出一辙。

扩展的作用是不求助于各种工具包甚至于 `jdk` 本身施舍给我们的各种方法，毕竟那是不可能的。

---
## 数据类（data class）

```kotlin
data class User(val name: String, val age: Int)
```

数据类通过具名参数使 `Builder 模式` 无用武之地：

```
User(name = "xin", age = 18)
```

这是我们第一次消除 java 的模式。👏

### 拷贝

有时候我们只需要修改数据类的某些值：

```
val xin = User(name = "xin", age = 18)
val olderXin = xin.copy(age = 19)
```

---
### 解构

```
val jane = User("Jane", 35) 
val (name, age) = jane
println("$name, $age years of age")
```

### 标准数据类

- `Pair` 
- `Triple`

---

## 密封类 （略过）

## 范型 （略过）

## 嵌套类（略过）

## 枚举类（略过）

## 对象表达式以及声明（略过）

## 委托 （略过）

## 委托属性（略过）

---

## 函数与Lambdas

### 函数

```
fun double(x: Int): Int {
    return 2 * x
}
```

#### 中缀符

```
infix fun Int.shl(x: Int): Int {
}

1 shl 2

1.shl(2)
```

---
#### 默认参数

kotlin 用默认参数替代了 java 方法重载：

```
fun read(b: Array<Byte>, off: Int = 0, len: Int = b.size()) {
}
```

#### 具名参数

我们可以在调用函数（方法）时参数具名：

```
"321".compareTo(other = "123") // other 是 compareTo 参数的名字
```

#### 单表达式函数

```
fun double(x: Int): Int = x * 2
```

---
## Lambdas

### 高阶函数

高阶函数就是携带参数为函数的函数：

```kotlin
fun <T> lock(lock: Lock, body: () -> T): T {
    lock.lock()
    try {
        return body()
    }
    finally {
        lock.unlock()
    }
}
```

---

如果高阶函数的最后一个参数为函数，那么高阶函数的调用将会有以下语法糖：
```
lock (lock) {
    sharedResource.operation()
}
```

#### Lambda 表达式语法

```
val sum = { x: Int, y: Int -> x + y } // 类型推断 (Int, Int) -> Int
```

#### 匿名函数

```
fun(x: Int, y: Int): Int = x + y
```

---

#### 闭包

Lambda 表达式或匿名函数都能访问它的闭包（变量在作用域外）：

```
var sum = 0
ints.filter { it > 0 }.forEach {
    sum += it // 与 java 不同的是闭包中捕获的变量可以修改
}
print(sum)
```

---

#### Receiver 函数字面量

```
class HTML {
    fun body() { ... }
}

fun html(init: HTML.() -> Unit): HTML {
    val html = HTML()  // 创建 Receiver 对象
    html.init()        // 传递 Receiver 对象给 Lambda
    return html
}


html {       // lambda
    body()   // 调用 HTML 方法
}
```

---

## Q && A


